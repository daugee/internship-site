<?php

session_start();

require_once 'vendor/autoload.php';

$app = new \Slim\Slim();

// config

require_once 'config.php';

// views

require_once 'views/helloworld.php';
require_once 'views/welcome.php';
require_once 'views/downloads.php';
require_once 'views/profiles.php';
require_once 'views/signup.php';

// routes

require_once 'routes.php';

$app->run();

?>
