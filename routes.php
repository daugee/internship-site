<?php

$app->get('/signup/student', 'student_signup')->name('signup_student');

$app->get('/profile/:name', 'profile')->name('profile_student');
$app->get('/profiles', 'profiles')->name('profiles');

$app->get('/downloads', 'downloads')->name('downloads');
$app->get('/hello','helloworld')->name('hello');
$app->get('/', 'welcome')->name('home');

?>